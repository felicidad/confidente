# Hapi Confidente

Creates configuration object stores based on criteria. Uses `confidence` to load up configurations. Passes `env` criteria by default with loads `NODE_ENV`. Refer to [Hapi Confidence](https://github.com/hapijs/confidence) for details on what this accomplishes.

```
npm i -s confidente
```

##### Usage

Without Criteria
``` js
// Create a new confugration object store

const myConfig = {
    $meta: "this is a sample conf",
    hasCache: {
        $filter: 'env',
        development: false,
        staging: false,
        $default: true
    }
}
const conf = new confidant(myConfig);

// Now you can use it
console.log(conf.get('/hasCache'));
// > false

console.log(conf.meta('/'));
// > "this is a sample conf"

```

With Criteria
``` js
// Create a new confugration object store

const criteria = {
    client: 'aaa'
}

const myConfig = {
    $meta: "this is a sample conf",
    hasCache: {
        $filter: 'env',
        development: false,
        staging: false,
        $default: true
    },
    showSlider: {
        $filter: 'client',
        aaa: true,
        $default: false
    }
}

const conf = new confidant(myConfig, criteria);

// Now you can use it
console.log(conf.get('/showSlider'));
// > true


// You can also override criteria on get
console.log(conf.get('/showSlider', { client: 'bbb' }));
// > false
```


##### Default criteria

The default criteria filters by `env` only, which defaults to `development` if not set.

``` js
{
    env: process.env.NODE_ENV || 'development'
}
```

##### Extend criteria

This function alters the internal criteria using `Object.assign()`. You can override `criteria.env` if you'd like with this function.

``` js
conf.setCriteria({
    buildNumber: process.env.BUILD_NUMBER
});
```

Now criteria will be:

``` js
{
    env: process.env.NODE_ENV || 'development',
    buildNumber: process.env.BUILD_NUMBER
}
```


const Hoek = require('hoek');
const Confidence = require('confidence');

class Confidente {

    constructor(props, criteria = {}) {

        Hoek.assert(props.constructor === Object, 'props must be an object');
        Hoek.assert(criteria.constructor === Object, 'criteria must be an object');

        this.store = new Confidence.Store(props);

        // Base criteria
        this.criteria = Object.assign({

            env: process.env.NODE_ENV || 'development'
        }, criteria);
    }

    get(key, criteria = {}) {

        return this.store.get(key, {
            ...this.criteria,
            ...criteria
        });
    }

    meta(key) {

        return this.store.meta(key, this.criteria);
    }

    // Extends criteria
    setCriteria(obj = {}) {

        Hoek.assert(obj.constructor === Object, 'criteria must be an object');
        Object.assign(this.criteria, obj);
    }
}

module.exports = Confidente;

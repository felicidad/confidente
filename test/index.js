// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');
const Confidence = require('confidence');

// App Modules
const Confidente = require('..');

// Module Variables
const { experiment, beforeEach, test } = exports.lab = Lab.script();
const { expect } = Code;

const stub = {

    config: {
        $meta: 'this is meta',
        test: {
            $filter: 'env',
            development: true,
            test: false,
            $default: '1'
        },
        lastName: {
            $filter: 'user',
            pepe: 'billete',
            papa: 'lote'
        }
    },
    criteria: {}
};

experiment('Confidant',  () => {

    beforeEach(() => {

        stub.conf = new Confidente(stub.config, stub.criteria);
    });

    test('it should instantiate a new object store with env criteria', () => {

        expect(stub.conf).to.be.an.object();
        expect(stub.conf.store).to.be.an.object();
        expect(stub.conf.store).to.be.an.instanceof(Confidence.Store);
        expect(stub.conf.criteria).to.be.an.object();
        expect(stub.conf.setCriteria).to.be.a.function();
        expect(stub.conf.get).to.be.a.function();
        expect(stub.conf.meta).to.be.a.function();

        delete process.env.NODE_ENV;
    });

    test('it should default criteria to development', () => {

        expect(stub.conf.criteria).to.include({ env: 'development' });
        process.env.NODE_ENV = 'test';
    });

    test('it should return config meta', () => {

        expect(stub.conf.meta('/')).to.equal('this is meta');

        stub.criteria = { user: 'pepe' };
    });

    test('it should accept a criteria', () => {

        expect(stub.conf.criteria.env).to.equal('test');
        expect(stub.conf.criteria.user).to.equal('pepe');

        expect(stub.conf.get('/test')).to.be.false();
        expect(stub.conf.get('/lastName')).to.equal('billete');
    });

    test('it should update a criteria', () => {

        expect(stub.conf.criteria.user).to.equal('pepe');

        expect(stub.conf.get('/test')).to.be.false();
        expect(stub.conf.get('/lastName')).to.equal('billete');

        stub.conf.setCriteria({ user: 'papa' });

        expect(stub.conf.criteria.user).to.equal('papa');

        expect(stub.conf.get('/lastName')).to.equal('lote');
    });

    test('it should overwrite criteria on get', () => {

        expect(stub.conf.criteria.user).to.equal('pepe');

        expect(stub.conf.get('/lastName')).to.equal('billete');
        expect(stub.conf.get('/lastName', { user: 'papa' })).to.equal('lote');
    });

});

